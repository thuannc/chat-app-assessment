import {
  ChatPage,
  LoginPage
} from './elements';
import { Navigate, useRoutes } from 'react-router-dom';

import AuthGuard from '../context/AuthGuard';

export default function Router() {
  return useRoutes([
    { element: <LoginPage />, index: true },
    {
      path: 'chat',
      element: (<AuthGuard />),
      children: [
        { element: <ChatPage />, index: true },
        { path: ':conversationId', element: <ChatPage /> },
        // { path: 'new', element: <ChatPage /> }, // TODO: create new conversation
      ],
    },
    { path: '404', element: <div>404</div> },
    { path: '*', element: <Navigate to="/404" replace /> },
  ]);
}
