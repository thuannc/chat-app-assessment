export const PATH_PAGE = {
  root: "/",
  chat: "/chat",
  page403: "/403",
  page404: "/404",
};
