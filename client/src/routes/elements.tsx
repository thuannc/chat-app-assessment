import { ElementType, Suspense, lazy } from 'react';

const Loadable = (Component: ElementType) => (props: any) =>
(
  <Suspense fallback={<div>Loading screen...</div>}>
    <Component {...props} />
  </Suspense>
);


export const LoginPage = Loadable(lazy(() => import('../pages/LoginPage')));
export const ChatPage = Loadable(lazy(() => import('../pages/ChatPage')));
