import { AvatarGroup } from '@mui/material';
import { CustomAvatarGroupProps } from './types';
import { forwardRef } from 'react';

const CustomAvatarGroup = forwardRef<HTMLDivElement, CustomAvatarGroupProps>(
  ({ size = 'small', compact, max, children, sx, ...other }, ref) => {

    return (
      <AvatarGroup
        ref={ref}
        max={compact ? 3 : max}
        spacing={'small'}
        sx={{
          '& .MuiAvatar-root': {
            width: 32,
            height: 32,
          },
          width: 40,
          height: 40,
          position: 'relative',
          '& .MuiAvatarGroup-avatar': {
            m: 0,
            width: 28,
            height: 28,
            position: 'absolute',
            '&:first-of-type': {
              left: 0,
              bottom: 0,
              zIndex: 9,
            },
            '&:last-of-type': {
              top: 0,
              right: 0,
            },
          },
          ...sx,
        }}
        {...other}
      >
        {children}
      </AvatarGroup>
    );
  }
);

export default CustomAvatarGroup;
