import { Box } from '@mui/material';
import { memo } from 'react';
import { styled } from '@mui/material/styles';

export const Scrollbar = styled(Box)(({ theme }) => ({
  flexGrow: 1,
  maxHeight: '100%',
  overflowY: 'auto',
  paddingLeft: 16,
  paddingRight: 16,
}));


export default memo(Scrollbar);
