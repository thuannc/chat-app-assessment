import { Manager } from 'socket.io-client';

export const DOMAIN = process.env.SOCKET_DOMAIN || 'http://localhost:4000';
export const UPLOAD_DOMAIN = `${DOMAIN}/upload`;

const socketManager = new Manager();

export default socketManager;
