import { formatDistanceToNow } from 'date-fns';

type InputValue = Date | string | number | null;

export function fToNow(date: InputValue) {
  return date
    ? formatDistanceToNow(new Date(date), {
        addSuffix: true,
      })
    : '';
}
