import {
  ThemeProvider as MUIThemeProvider,
  StyledEngineProvider,
  ThemeOptions,
  createTheme,
} from '@mui/material/styles';

import { CssBaseline } from '@mui/material';
import GlobalStyles from './globalStyles';
import componentsOverride from './overrides';
import palette from './palette';
import shadows from './shadows';
import typography from './typography';
import { useMemo } from 'react';

type Props = {
  children: React.ReactNode;
};

export default function ThemeProvider({ children }: Props) {

  const themeOptions: ThemeOptions = useMemo(
    () => ({
      palette: palette(),
      typography,
      shape: { borderRadius: 8 },
      shadows: shadows(),
    }),
    []
  );

  const theme = createTheme(themeOptions);

  theme.components = componentsOverride(theme);

  return (
    <StyledEngineProvider injectFirst>
      <MUIThemeProvider theme={theme}>
        <CssBaseline />
        <GlobalStyles />
        {children}
      </MUIThemeProvider>
    </StyledEngineProvider>
  );
}
