import Avatar from './Avatar';
import Badge from './Badge';
import Button from './Button';
import Card from './Card';
import LoadingButton from './LoadingButton';
import Popover from './Popover';
import Skeleton from './Skeleton';
import SvgIcon from './SvgIcon';
import { Theme } from '@mui/material/styles';
import Typography from './Typography';

export default function ComponentsOverrides(theme: Theme) {
  return Object.assign(
    Card(theme),
    Badge(theme),
    Button(theme),
    Avatar(theme),
    Popover(theme),
    SvgIcon(theme),
    Skeleton(theme),
    Typography(theme),
    LoadingButton(theme)
  );
}
