import { AuthContext } from './AuthProvider';
import { useContext } from 'react';

export const useAuthProvider = () => {
  const context = useContext(AuthContext);

  if (!context) throw new Error('useAuthProvider context must be use inside AuthProvider');

  return context;
};
