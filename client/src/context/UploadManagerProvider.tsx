import { createContext, useCallback, useMemo, useState } from 'react';

import { io } from 'socket.io-client';
import { UPLOAD_DOMAIN } from '../utils/socketManager';
import { IChatSendMessage } from '../types/chat';
import chatEvents from '../constant/chatEvents';

export const UploadContext = createContext<any>({});

type UploadProviderProps = {
    children: React.ReactNode;
};

interface IUploadState extends IChatSendMessage {
    uploadStatus: string
}

/**
 * Ý tưởng là quản lí tiến trình upload theo messageID với từng connection riêng
 * sau khi upload xong sẽ close socket tương ứng và callback
*/

export function UploadProvider({ children }: UploadProviderProps) {
    const [uploadState, setUploadState] = useState<Record<string, IUploadState>>({});

    const updateStatus = useCallback((messageId: string, status: string) => {
        setUploadState(prev => {
            const prevRecord = prev[messageId] || {};
            return ({ ...prev, [messageId]: { ...prevRecord, uploadStatus: status } });
        })
    }, [])

    const doUpload = useCallback(({ chatSendMessage, onSuccess }: { chatSendMessage: IChatSendMessage | any, onSuccess?: (messageId: string, status: string) => void }) => {
        const { attachments, messageId } = chatSendMessage;

        const newUploadSocket = io(UPLOAD_DOMAIN, { forceNew: true }); // create new another connection

        newUploadSocket.connect();
        newUploadSocket.emit(chatEvents.CHAT_ATTACHMENT_SEND, attachments, (status: string) => {
            updateStatus(messageId, status);
            newUploadSocket.close();
            onSuccess?.(messageId, status);
        });

        const newRecord = { ...chatSendMessage, uploadStatus: 'uploading' } as IUploadState;
        setUploadState(prev => ({ ...prev, [messageId]: newRecord }))
    }, [updateStatus])

    const getStatus = useCallback((messageId: string) => {
        return uploadState[messageId];
    }, [uploadState])

    const memoizedValue = useMemo(
        () => ({
            uploadState,
            doUpload,
            getStatus
        }),
        [doUpload, uploadState, getStatus]
    );

    return <UploadContext.Provider value={memoizedValue}>{children}</UploadContext.Provider>;
}
