import { SocketContext } from './SocketProvider';
import { useContext } from 'react';

export const useSocketProvider = () => {
  const context = useContext(SocketContext);

  if (!context) throw new Error('useSocketProvider context must be use inside ChatProvider');

  return context;
};
