import { UploadContext } from './UploadManagerProvider';
import { useContext } from 'react';

export const useUploadProvider = () => {
  const context = useContext(UploadContext);

  if (!context)
    throw new Error(
      'useUploadProvider context must be use inside ChatProvider'
    );

  return context;
};
