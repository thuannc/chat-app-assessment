import { Navigate, Outlet } from 'react-router-dom';

import { useAuthProvider } from './useAuthProvider';

type AuthGuardProps = {
    children?: React.ReactNode;
};

/**
 * Wrapper component giúp chặn truy cập khi chưa auth (phiên bản sơ) 
*/
export default function AuthGuard({ children }: AuthGuardProps) {
    const { user } = useAuthProvider();

    if (!user?.id) {
        return <Navigate to='/' />
    }

    return (
        <>
            <Outlet />
            {children}
        </>
    );
}
