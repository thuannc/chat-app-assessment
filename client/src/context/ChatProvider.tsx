import { IChatConversationsState, IChatParticipant, IChatState } from '../types/chat';
import { createContext, useEffect, useMemo } from 'react';

import { useAuthProvider } from './useAuthProvider';
import useOnConversationsIO from '../module/chat/services/useOnConversationsIO';
import { useParams } from 'react-router-dom';
import { useSocketProvider } from './useSocketProvider';
import chatEvents from '../constant/chatEvents';
import useOnMessagesIO from '../module/chat/services/useOnMessagesIO';

const initialState: IChatState = {
  messages: [],
  conversations: { byId: {}, allIds: [] },
  activeConversationId: null,
  participants: [],
};

interface IChatProviderState extends IChatState { }

export const ChatContext = createContext<IChatProviderState>(initialState);

type ChatProviderProps = {
  children: React.ReactNode;
};
/**
 * cung cấp các state của luổng chat và các method liên quan
*/
export function ChatProvider({ children }: ChatProviderProps) {
  const { conversationId = '' } = useParams(); // conversationId === roomId
  const { user } = useAuthProvider();
  const { chatSocket } = useSocketProvider();
  const { messages, emitGetMessages } = useOnMessagesIO()

  useEffect(() => {
    chatSocket.emit(chatEvents.JOIN_ROOM, { userId: user.id, userName: user.name, conversationId });
  }, [conversationId, user, chatSocket])

  useEffect(() => {
    emitGetMessages(conversationId, user.id);
  }, [conversationId, user, emitGetMessages])

  const conversations: IChatConversationsState = useOnConversationsIO();

  const participants: IChatParticipant[] = useMemo(() => {
    if (!conversationId || !conversations) return [];
    return conversations?.byId[conversationId]?.participants || []
  }, [conversations, conversationId]);

  const memoizedValue = useMemo(
    () => ({
      activeConversationId: conversationId,
      conversations,
      messages,
      participants,
    }),
    [conversations, conversationId, participants, messages]
  );

  return <ChatContext.Provider value={memoizedValue}>{children}</ChatContext.Provider>;
}
