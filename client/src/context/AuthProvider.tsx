import { createContext, useCallback, useEffect, useMemo, useState } from 'react';

import { useNavigate } from 'react-router-dom';

export const AuthContext = createContext<any>({});

type AuthProviderProps = {
  children: React.ReactNode;
};

const DUMP_USER_ID = 'DUMP_USER_ID';
/**
 * cung cấp các phương thức auth cũng như lưu thông tin user
*/
export function AuthProvider({ children }: AuthProviderProps) {
  const navigate = useNavigate();

  const [user, setUser] = useState({ name: '', id: DUMP_USER_ID });

  // call login API, set userinfo and callback when success
  const doLogin = useCallback((userName: string, onSuccess?: () => void) => {
    setUser(prev => ({ ...prev, name: userName }));
    localStorage.setItem('user', userName);
    onSuccess?.();
  }, [])

  const login = useCallback(async (userName: string) => {
    doLogin(userName, () => navigate('chat'));
  }, [doLogin, navigate]);

  const logout = useCallback(() => {
    setUser(prev => ({ ...prev, name: '' }));
    localStorage.removeItem('user');
  }, []);

  // check has token
  const initialize = useCallback(async () => {
    try {
      const userName = localStorage.getItem('user') || '';
      doLogin(userName);
    } catch (error) {
      console.error(error);
    }
  }, [doLogin]);

  useEffect(() => {
    initialize();
  }, [initialize]);

  const memoizedValue = useMemo(
    () => ({
      user,
      login,
      logout,
    }),
    [login, logout, user]
  );

  return <AuthContext.Provider value={memoizedValue}>{children}</AuthContext.Provider>;
}
