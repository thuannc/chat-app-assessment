import { createContext, useEffect, useMemo, } from 'react';

import socketManager from '../utils/socketManager';
import { Manager, Socket } from 'socket.io-client';

interface ISocketContextState {
  socket: Socket,
  chatSocket: Socket,
  socketManager: Manager
}

export const SocketContext = createContext<ISocketContextState>({
  socket: socketManager.socket('/'),
  chatSocket: socketManager.socket("/chat"),
  socketManager: socketManager
});

type SocketProviderProps = {
  children: React.ReactNode;
};

export function SocketProvider({ children }: SocketProviderProps) {
  const { socket, chatSocket } = useMemo(() => {
    return {
      socket: socketManager.socket('/'),
      chatSocket: socketManager.socket("/chat")
    }
  }, [])

  useEffect(() => {
    chatSocket.connect();
    chatSocket.on('connect', () => console.log('chatSocket connected!'));
    return () => {
      chatSocket.disconnect();
    }
  }, [chatSocket])

  const memoizedValue = useMemo(
    () => ({
      socket,
      chatSocket,
      socketManager
    }),
    [socket, chatSocket]
  );

  return <SocketContext.Provider value={memoizedValue}>{children}</SocketContext.Provider>;
}
