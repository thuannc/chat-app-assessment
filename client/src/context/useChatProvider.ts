import { ChatContext } from './ChatProvider';
import { useContext } from 'react';

export const useChatProvider = () => {
  const context = useContext(ChatContext);

  if (!context) throw new Error('useChatProvider context must be use inside ChatProvider');

  return context;
};
