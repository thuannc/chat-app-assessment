import { useEffect, useMemo, useState } from 'react';
import { Card, Stack } from '@mui/material';

import ChatHeaderDetail from './components/header/ChatHeaderDetail';
import ChatMessageInput from './components/message/ChatMessageInput';
import ChatMessageList from './components/message/ChatMessageList';
import ChatNav from './components/nav/ChatNav';
import { ChatProvider } from '../../context/ChatProvider';
import { IChatMessage, IChatSendMessage } from '../../types/chat';
import { useAuthProvider } from '../../context/useAuthProvider';
import { useChatProvider } from '../../context/useChatProvider';
import { useSocketProvider } from '../../context/useSocketProvider';
import chatEvents from '../../constant/chatEvents';
import useOnMessagesIO from './services/useOnMessagesIO';
import { useUploadProvider } from '../../context/useUploadProvider';

function Chat() {
  const { user } = useAuthProvider();
  const { chatSocket } = useSocketProvider();
  const { conversations, activeConversationId, messages, participants } = useChatProvider();
  const { emitGetMoreMessages } = useOnMessagesIO();
  const { doUpload } = useUploadProvider();

  const [latestMessage, setLatestMessage] = useState<IChatMessage[]>([]);

  const selectedConversation = useMemo(() => {
    if (activeConversationId) {

      const activeConversation = conversations.byId[activeConversationId];
      const currentMessages = activeConversation.messages;
      return {
        ...activeConversation,
        messages: [...messages, ...currentMessages, ...latestMessage], // TODO: combine with latestMessage, uniq message list by messageId (lodash uniq)
      };
    }
    return {
      id: '',
      messages: [],
      participants: [],
      unreadCount: 0,
      type: '',
    };
  }, [activeConversationId, conversations, messages, latestMessage]);

  const displayParticipants = (participants || []).filter((item: any) => item.id !== user.id);

  const onSendMessage = async (value: IChatSendMessage) => {
    try {
      const { messageId, senderId, message, contentType, createdAt } = value;
      setLatestMessage(prev => ([...prev, {
        id: messageId,
        body: message,
        contentType,
        createdAt,
        senderId,
        attachments: []
      } as IChatMessage]))
      chatSocket.emit(chatEvents.CHAT_SEND, value);
    } catch (error) {
      console.error(error);
    }
  };

  const onSendFile = async (value: IChatSendMessage, file: any) => {
    try {
      const { messageId, senderId, message, contentType, createdAt } = value;
      const data = {
        id: messageId,
        body: message,
        contentType,
        createdAt,
        senderId,
        attachments: [file]
      }

      doUpload({
        chatSendMessage: data, onSuccess: () => {
          chatSocket.emit(chatEvents.CHAT_SEND, value);
        }
      })

      setLatestMessage(prev => ([...prev, {
        ...data, attachments: []
      } as IChatMessage]))

    } catch (error) {
      console.error(error);
    }
  };

  const onLoadmore = async () => {
    emitGetMoreMessages(activeConversationId as never, user.id);
  };

  useEffect(() => { setLatestMessage([]) }, [activeConversationId])

  return (
    <Stack sx={{ height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center', px: 8 }}>
      <Card sx={{ height: '72vh', width: '100%', display: 'flex' }}>
        <ChatNav />
        <Stack flexGrow={1}>
          <ChatHeaderDetail participants={displayParticipants} />
          <Stack
            direction="row"
            flexGrow={1}
            sx={{
              overflow: 'hidden',
              borderTop: (theme) => `solid 1px ${theme.palette.divider}`,
            }}
          >
            <Stack flexGrow={1} sx={{ mt: 2 }}>
              <ChatMessageList onLoadmore={onLoadmore} conversation={selectedConversation} />
              <ChatMessageInput
                conversationId={activeConversationId}
                onSend={onSendMessage}
                onSendFile={onSendFile}
              />
            </Stack>
          </Stack>
        </Stack>
      </Card>
    </Stack >
  );
}

function ChatWrapper() {
  return <ChatProvider><Chat /></ChatProvider>
}

export default ChatWrapper;