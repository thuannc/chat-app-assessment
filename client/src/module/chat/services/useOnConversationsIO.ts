import { useEffect, useMemo, useState } from 'react';

import { IChatConversation } from '../../../types/chat';
import conversationsDump from '../../../mock/conversation';
import keyBy from 'lodash/keyBy';
import { useSocketProvider } from '../../../context/useSocketProvider';
import chatEvents from '../../../constant/chatEvents';

const useOnConversationsIO = () => {
  const { chatSocket } = useSocketProvider();
  const [conversations, setConversations] = useState<IChatConversation[]>([]);

  useEffect(() => {
    chatSocket.on(chatEvents.CONVERSATIONS, (data: IChatConversation[]) => {
      setConversations(data);
    });
    return () => {
      chatSocket.off(chatEvents.CONVERSATIONS);
    };
  }, [chatSocket]);

  return useMemo(() => {
    const data = conversations.length > 0 ? conversations : conversationsDump; // TODO: mock data
    const byId = keyBy(data, 'id'); // id === conversationId
    return {
      byId,
      allIds: Object.keys(byId)
    };
  }, [conversations]);
};

export default useOnConversationsIO;
