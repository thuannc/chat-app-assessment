import { useCallback, useEffect, useMemo, useState } from 'react';

import { IChatMessage } from '../../../types/chat';
import { useSocketProvider } from '../../../context/useSocketProvider';
import chatEvents from '../../../constant/chatEvents';

const useOnMessagesIO = () => {
  const { chatSocket } = useSocketProvider();
  const [messages, setMessages] = useState<IChatMessage[]>([]);

  useEffect(() => {
    chatSocket.on(chatEvents.CHAT_ON_MESSAGES, (data: IChatMessage[]) => {
      setMessages(data); // TODO: update pagination hanler
    });
    return () => {
      chatSocket.off(chatEvents.CHAT_ON_MESSAGES);
    };
  }, [chatSocket]);

  const emitGetMessages = useCallback(
    (conversationId: string, senderId: string) => {
      chatSocket.emit(chatEvents.CHAT_EMIT_MESSAGES, {
        conversationId,
        senderId
      });
    },
    [chatSocket]
  );

  const emitGetMoreMessages = useCallback(
    (conversationId: string, senderId: string) => {
      chatSocket.emit(chatEvents.CHAT_LOADMORE_MESSAGE, {
        conversationId,
        senderId
      });
    },
    [chatSocket]
  );

  return useMemo(
    () => ({
      messages,
      emitGetMessages,
      emitGetMoreMessages
    }),
    [messages, emitGetMessages, emitGetMoreMessages]
  );
};

export default useOnMessagesIO;
