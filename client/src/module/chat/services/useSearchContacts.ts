import { useCallback } from 'react';

// TODO: q is query params
const fetchData = async (q: string) => {
  const res = await fetch('/api/contacts.json')
    .then(res => res.json())
    .catch(err => {
      console.log('🚀 useSearchContacts ~ err:', err);
    });
  return res;
};

const useSearchContacts = () => {
  const searchContact = useCallback(async (q: string) => {
    const res = await fetchData(q);
    return res.contacts || [];
  }, []);

  return searchContact;
};

export default useSearchContacts;
