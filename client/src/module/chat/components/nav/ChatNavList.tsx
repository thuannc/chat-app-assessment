import { List, SxProps, Typography } from '@mui/material';

import ChatNavItem from './ChatNavItem';
import { IChatConversationsState } from '../../../../types/chat';
import { useAuthProvider } from '../../../../context/useAuthProvider';
import { useNavigate } from 'react-router-dom';

type Props = {
  conversations: IChatConversationsState;
  openNav: boolean;
  onCloseNav: VoidFunction;
  selected: (conversationId: string) => boolean;
  sx?: SxProps;
};

export default function ChatNavList({
  conversations,
  openNav,
  onCloseNav,
  selected,
  sx,
  ...other
}: Props) {
  const navigate = useNavigate();
  const { user } = useAuthProvider()

  const handleSelectConversation = (conversationId: string) => {
    let conversationIdTemp = '';
    const conversation = conversations.byId[conversationId];

    if (conversation.type === 'GROUP') {
      conversationIdTemp = conversation.id;
    } else {
      const otherParticipant = conversation.participants.find(
        (participant) => participant.id !== user.id
      );
      conversationIdTemp = otherParticipant?.id || '';
    }
    console.log('🍉 conversationIdTemp:', conversationIdTemp);
    navigate(`/chat/${conversationIdTemp}`);
  };

  const loading = !conversations.allIds.length;

  return (
    <List disablePadding sx={sx} {...other}>

      {loading ? <Typography>Loading...</Typography> : conversations.allIds.map((conversationId, index) => <ChatNavItem
        key={conversationId}
        openNav={openNav}
        conversation={conversations.byId[conversationId]}
        isSelected={selected(conversationId)}
        onSelect={() => handleSelectConversation(conversationId)}
      />)}
    </List>
  );
}
