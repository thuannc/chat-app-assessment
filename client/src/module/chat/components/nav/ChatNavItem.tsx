import {
  Avatar,
  Badge,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Stack,
  Typography
} from '@mui/material';

import { CustomAvatarGroup } from '../../../../components/custom-avatar';
import { IChatConversation } from '../../../../types/chat';
import { formatDistanceToNowStrict } from 'date-fns';
import { useAuthProvider } from '../../../../context/useAuthProvider';

type Props = {
  conversation: IChatConversation;
  openNav: boolean;
  isSelected: boolean;
  onSelect: VoidFunction;
};

const getDetails = (conversation: IChatConversation, currentUserId: string) => {
  const otherParticipants = (conversation?.participants || []).filter(
    (participant) => participant.id !== currentUserId
  );

  const displayNames = otherParticipants.map((participant) => participant.name).join(', ');

  let displayText = '';

  const lastMessage = conversation.messages[conversation.messages.length - 1];
  if (lastMessage) {
    const sender = lastMessage.senderId === currentUserId ? 'You: ' : '';

    displayText = `${sender}${lastMessage.body}`;
  }
  return { otherParticipants, displayNames, displayText };
};

export default function ChatNavItem({ conversation, openNav, isSelected, onSelect }: Props) {
  const { user } = useAuthProvider();
  const details = getDetails(conversation, user?.id as string);

  const lastActivity = conversation.messages[conversation.messages.length - 1].createdAt;

  const isGroup = details.otherParticipants.length > 1;

  return (
    <ListItemButton
      disableGutters
      onClick={onSelect}
      sx={{
        py: 1.5,
        px: 2.5,
        ...(isSelected && {
          bgcolor: 'action.selected',
        }),
      }}
    >
      <ListItemAvatar>
        {isGroup ? (
          <Badge
            overlap="circular"
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          >
            <CustomAvatarGroup compact sx={{ width: 40, height: 40 }}>
              {details.otherParticipants.slice(0, 2).map((participant) => (
                <Avatar
                  key={participant.id}
                  alt={participant.name}
                  src={participant.avatarUrl}
                />
              ))}
            </CustomAvatarGroup>
          </Badge>
        ) : (
          <Avatar
            key={details.otherParticipants[0].id}
            alt={details.otherParticipants[0].name}
            src={details.otherParticipants[0].avatarUrl}
            sx={{ width: 48, height: 48 }}
          />
        )}
      </ListItemAvatar>

      {openNav && (
        <>
          <ListItemText
            primary={details.displayNames}
            primaryTypographyProps={{ noWrap: true, variant: 'subtitle2' }}
            secondary={details.displayText}
            secondaryTypographyProps={{
              noWrap: true,
              variant: 'body2',
              color: 'text.secondary',
            }}
          />

          <Stack alignItems="flex-end" sx={{ ml: 2, height: 44 }}>
            <Typography
              noWrap
              variant="body2"
              component="span"
              sx={{
                mb: 1.5,
                fontSize: 12,
                color: 'text.disabled',
              }}
            >
              {formatDistanceToNowStrict(new Date(lastActivity), {
                addSuffix: false,
              })}
            </Typography>


          </Stack>
        </>
      )}
    </ListItemButton>
  );
}




