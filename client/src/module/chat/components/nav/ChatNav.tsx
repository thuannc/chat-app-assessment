import { Box, Stack } from '@mui/material';
import { useCallback, useState } from 'react';

import ChatNavList from './ChatNavList';
import ChatNavSearch from './ChatNavSearch';
import ChatNavSearchResults from './ChatNavSearchResults';
import { IChatContact, } from '../../../../types/chat';
import Scrollbar from '../../../../components/scrollbar';
import { useChatProvider } from '../../../../context/useChatProvider';
import { useNavigate } from 'react-router-dom';
import useSearchContacts from '../../services/useSearchContacts';

const NAV_WIDTH = 320;


export default function ChatNav() {
  const navigate = useNavigate();
  const searchContact = useSearchContacts();

  const [openNav, setOpenNav] = useState(true);
  const [searchResults, setSearchResults] = useState([]);
  const [searchValue, setSearchValue] = useState('');

  const { conversations, activeConversationId } = useChatProvider();

  const handleCloseNav = () => {
    setOpenNav(false);
  };

  // TODO: add debounce to search
  const handleChangeSearch = useCallback(async (v: string) => {
    try {

      if (v) {
        const response = await searchContact(v);
        setSearchResults(response);
      } else {
        setSearchResults([]);
      }
    } catch (error) {
      console.error(error);
    }
  }, [searchContact,]);

  const onSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setSearchValue(value);
    handleChangeSearch(value)
  }

  const handleSelectContact = (result: IChatContact) => {
    setSearchValue('');
    navigate(`chat/${result.id}`);
  };

  return (
    <Stack sx={{ width: NAV_WIDTH, }}>
      <Box sx={{ p: 2.5 }}>
        <ChatNavSearch
          value={searchValue}
          onChange={onSearch}
          onClickAway={() => setSearchValue('')}
        />
      </Box>
      <Scrollbar>
        {!searchValue ? (
          <ChatNavList
            openNav={openNav}
            onCloseNav={handleCloseNav}
            conversations={conversations}
            selected={(conversationId: string) => activeConversationId === conversationId}
          />
        ) : (
          <ChatNavSearchResults
            searchContacts={searchValue}
            searchResults={searchResults}
            onSelectContact={handleSelectContact}
          />
        )}
        {!searchValue ? (
          <ChatNavList
            openNav={openNav}
            onCloseNav={handleCloseNav}
            conversations={conversations}
            selected={(conversationId: string) => activeConversationId === conversationId}
          />
        ) : (
          <ChatNavSearchResults
            searchContacts={searchValue}
            searchResults={searchResults}
            onSelectContact={handleSelectContact}
          />
        )}
      </Scrollbar>
    </Stack>
  );
}
