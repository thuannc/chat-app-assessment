import { Avatar, AvatarGroup, Box, Stack, Typography } from '@mui/material';

import { IChatParticipant } from '../../../../types/chat';
import { fToNow } from '../../../../utils/formatTime';

type Props = {
  participants: IChatParticipant[];
};

export default function ChatHeaderDetail({ participants }: Props) {
  const isGroup = participants.length > 1;

  const participantInfo = participants.length ? participants[0] : null;

  return (
    <Stack
      direction="row"
      alignItems="center"
      sx={{
        p: (theme) => theme.spacing(2, 1, 2, 2),

      }}
    >
      {isGroup ? (
        <Stack flexGrow={1}>
          <Stack direction="row" alignItems="center" spacing={2}>
            <AvatarGroup max={3}>
              {participants.map((participant) => (
                <Avatar key={participant.id} alt={participant.name} src={participant.avatarUrl} />
              ))}
            </AvatarGroup >
            <Typography variant="body2" sx={{ color: 'text.secondary' }}>{participants.length} persons</Typography>
          </Stack>
        </Stack>
      ) : (
        <Stack flexGrow={1} direction="row" alignItems="center" spacing={2}>
          <Avatar
            src={participantInfo?.avatarUrl}
            alt={participantInfo?.name}
          />
          <div>
            <Typography variant="subtitle2">{participantInfo?.name}</Typography>

            <Typography variant="body2" sx={{ color: 'text.secondary' }}>
              {participantInfo?.status === 'offline' ? (
                participantInfo?.lastActivity && fToNow(participantInfo?.lastActivity)
              ) : (
                <Box component="span" sx={{ textTransform: 'capitalize' }}>
                  {participantInfo?.status}
                </Box>
              )}
            </Typography>
          </div>
        </Stack>
      )}
    </Stack>
  );
}
