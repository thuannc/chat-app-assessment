import { IconButton, InputBase, InputBaseProps, Stack } from '@mui/material';
import { useRef, useState } from 'react';

import { IChatSendMessage } from '../../../../types/chat';
import Iconify from '../../../../components/iconify';
import { useAuthProvider } from '../../../../context/useAuthProvider';
import uuidv4 from '../../../../utils/uuidv4';
import { useSocketProvider } from '../../../../context/useSocketProvider';
import chatEvents from '../../../../constant/chatEvents';

interface Props extends InputBaseProps {
  conversationId: string | null;
  onSend: (data: IChatSendMessage) => void;
  onSendFile: (data: IChatSendMessage, file: any) => void;
}

export default function ChatMessageInput({
  conversationId,
  onSend,
  onSendFile,
  sx,
  ...other
}: Props) {
  const { user } = useAuthProvider();
  const { chatSocket } = useSocketProvider();

  const fileInputRef = useRef<HTMLInputElement>(null);

  const [message, setMessage] = useState('');

  const getLocalUrl = async (file: any) => {
    const url = await new Promise(resolve => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result as string);
    });
    return url as string;
  }

  const onChangeFile = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const { files } = event.target;
    if (files?.[0] && onSendFile && conversationId) {
      const url: string = await getLocalUrl(files?.[0]);
      onSendFile({
        conversationId,
        messageId: uuidv4(),
        message: url,
        contentType: 'image',
        attachments: [],
        createdAt: new Date(),
        senderId: user?.id,
      }, files?.[0]);
    }
  }

  const handleClickAttach = () => {
    fileInputRef.current?.click();
  };

  const handleSend = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      if (onSend && message && conversationId) {
        onSend({
          conversationId,
          messageId: uuidv4(),
          message,
          contentType: 'text',
          attachments: [],
          createdAt: new Date(),
          senderId: user?.id,
        });
      }
      setMessage('');
    }
  };

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMessage(event.target.value);
    chatSocket.emit(chatEvents.CHAT_TYPING, { senderId: user.id, userName: user.name, conversationId });
  }

  return (
    <>
      <InputBase
        value={message}
        onKeyUp={handleSend}
        onChange={onChange}
        placeholder="Type a message"
        endAdornment={
          <Stack direction="row" spacing={1} sx={{ flexShrink: 0, mr: 1.5 }}>
            <IconButton size="small" disabled={!conversationId} onClick={handleClickAttach}>
              <Iconify icon="ic:round-add-photo-alternate" />
            </IconButton>
          </Stack>
        }
        sx={{
          pl: 1,
          height: 56,
          flexShrink: 0,
          borderTop: (theme) => `solid 1px ${theme.palette.divider}`,
          ...sx,
        }}
        {...other}
      />

      <input type="file" ref={fileInputRef} style={{ display: 'none' }} onChange={onChangeFile} />
    </>
  );
}
