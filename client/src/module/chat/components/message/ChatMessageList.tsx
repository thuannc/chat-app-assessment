import { useEffect, useRef } from 'react';

import ChatMessageItem from './ChatMessageItem';
import { IChatConversation } from '../../../../types/chat';
import Scrollbar from '../../../../components/scrollbar/Scrollbar';

type Props = {
  conversation: IChatConversation;
  onLoadmore?: () => void
};

export default function ChatMessageList({ conversation }: Props) {
  const scrollRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const scrollMessagesToBottom = () => {
      if (scrollRef.current) {
        scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
      }
    };
    scrollMessagesToBottom();
  }, [conversation.messages]);

  return (
    <Scrollbar ref={scrollRef}>
      {conversation.messages.map((message) => (
        <ChatMessageItem
          key={message.id}
          message={message}
          conversation={conversation}
        />
      ))}
    </Scrollbar>
  );

}
