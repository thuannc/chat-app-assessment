import { Button, Card, Container, Stack, TextField } from '@mui/material';

import { useAuthProvider } from '../../context/useAuthProvider';
import { useState } from 'react'

export default function Login() {
    const { login } = useAuthProvider();
    const [name, setName] = useState('');

    const onLogin = () => {
        if (name) login(name)
    }

    return (
        <Container sx={{ height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Card sx={{ p: 4 }}>
                <Stack spacing={3} >
                    <TextField
                        fullWidth
                        value={name}
                        onChange={e => setName(e.target.value)}
                        label='Your name'
                    />
                    <Button
                        fullWidth
                        size="large"
                        variant="contained"
                        onClick={onLogin}
                    >
                        Join Chat
                    </Button>
                </Stack>
            </Card>
        </Container>
    );
}
