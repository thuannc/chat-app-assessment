export type IChatAttachment = {
  id: string;
  name: string;
  size: number;
  type: string;
  path: string;
  preview: string;
  createdAt: Date | string | number;
  modifiedAt: Date | string | number;
};

export type IChatTextMessage = {
  id: string;
  body: string;
  contentType: 'text';
  attachments: IChatAttachment[];
  createdAt: Date | string | number;
  senderId: string;
};

export type IChatImageMessage = {
  id: string;
  body: string;
  contentType: 'image';
  attachments: IChatAttachment[];
  createdAt: Date | string | number;
  senderId: string;
};

export type IChatMessage = IChatTextMessage | IChatImageMessage;

export type IChatContact = {
  id: string;
  name: string;
  username?: string;
  avatar: string;
  lastActivity: Date | string | number;
  status: string;
};

export type IChatParticipant = {
  id: string;
  name: string;
  username?: string;
  avatarUrl: string;
  lastActivity?: Date | string | number;
  status?: 'online' | 'offline';
};

export type IChatConversation = {
  id: string;
  participants: IChatParticipant[];
  type: string;
  unreadCount: number;
  messages: IChatMessage[];
};

export type IChatSendMessage = {
  conversationId: string;
  messageId: string;
  message: string;
  contentType: 'text' | 'image';
  attachments: string[];
  createdAt: Date | string | number;
  senderId: string;
};

export type IChatContactsState = {
  byId: Record<string, IChatParticipant>;
  allIds: string[];
};

export type IChatConversationsState = {
  byId: Record<string, IChatConversation>;
  allIds: string[];
};

export interface IChatState {
  activeConversationId: null | string;
  conversations: IChatConversationsState;
  messages: IChatMessage[];
  participants: IChatParticipant[];
}
