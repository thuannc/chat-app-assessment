import { IChatConversation } from '../types/chat';

const conversations: IChatConversation[] = [
  {
    id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2',
    participants: [
      {
        status: 'online',
        id: 'DUMP_USER_ID',
        name: 'Jaydon Frankie',
        lastActivity: '2023-07-02T16:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-sunglasses_23-2149436188.jpg?w=2000'
      },
      {
        status: 'online',
        id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2',
        name: 'Lucian Obrien',
        lastActivity: '2023-07-01T15:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-sunglasses_23-2149436188.jpg?w=2000'
      }
    ],
    type: 'ONE_TO_ONE',
    unreadCount: 0,
    messages: [
      {
        id: 'eabe575e-286a-4824-ada0-7741040f202d',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T06:26:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2'
      },
      {
        id: 'cce0dcc9-7e17-47ef-a5ce-996416294231',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T14:26:02.673Z',
        senderId: 'DUMP_USER_ID'
      }
    ]
  },
  {
    id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3',
    participants: [
      {
        status: 'online',
        id: 'DUMP_USER_ID',
        name: 'Jaydon Frankie',
        lastActivity: '2023-07-02T16:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-glasses-half-shaved-head_23-2149436187.jpg'
      },
      {
        status: 'offline',
        id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3',
        name: 'Deja Brady',
        lastActivity: '2023-06-30T14:26:02.672Z',
        avatarUrl:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfHc7y8Xqsn-0MpB7QnbfNzJ9GJL2JdwIJbI7syEuVPxEqInk4gMc719gC7TPmkr5ewm4&usqp=CAU'
      }
    ],
    type: 'ONE_TO_ONE',
    unreadCount: 0,
    messages: [
      {
        id: '41a3386b-a2cd-4b5c-ab22-6d14f210d160',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T08:26:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3'
      },
      {
        id: '5c7677a4-2e2a-41ab-95e4-6e1b8212b4d3',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T10:26:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: 'd2b69b51-fe91-4fb4-83f2-aa414b62578e',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T11:56:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b3'
      },
      {
        id: '20ca4a10-ee37-4cfb-963b-885ae5395661',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T14:11:02.673Z',
        senderId: 'DUMP_USER_ID'
      }
    ]
  },
  {
    id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4',
    participants: [
      {
        status: 'online',
        id: 'DUMP_USER_ID',
        name: 'Jaydon Frankie',
        lastActivity: '2023-07-02T16:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-glasses-half-shaved-head_23-2149436187.jpg'
      },
      {
        status: 'online',
        id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4',
        name: 'Harrison Stein',
        lastActivity: '2023-06-29T13:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-sunglasses_23-2149436178.jpg'
      }
    ],
    type: 'ONE_TO_ONE',
    unreadCount: 0,
    messages: [
      {
        id: '6ed72d67-b277-420c-953b-fb7c4cb568a6',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T08:26:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4'
      },
      {
        id: 'f32f24c2-f020-4b6c-b33f-c7ff35330da8',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T10:26:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: '379e3324-9df6-49ed-9df5-cfebda9e6bb8',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T11:56:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4'
      },
      {
        id: 'e8f2e917-397a-4754-a3bc-f90d7b5253ea',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T14:11:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: '4bf7d07a-8cc9-4ba3-8b09-c422562b50b6',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry..',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T15:11:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4'
      },
      {
        id: 'e8f2e917-397a-4754-a3bc-f90d7b5253ew',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T14:11:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: '4bf7d07a-8cc9-4ba3-8b09-c422562b50b7',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T15:11:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b4'
      }
    ]
  },
  {
    id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5',
    participants: [
      {
        status: 'online',
        id: 'DUMP_USER_ID',
        name: 'Jaydon Frankie',
        lastActivity: '2023-07-02T16:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-glasses-half-shaved-head_23-2149436187.jpg'
      },
      {
        status: 'offline',
        id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5',
        name: 'Reece Chung',
        lastActivity: '2023-06-28T12:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-glasses_23-2149436190.jpg'
      }
    ],
    type: 'ONE_TO_ONE',
    unreadCount: 8,
    messages: [
      {
        id: '7356b530-24ef-4ed3-bb47-ef9cb573fabf',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T06:26:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5'
      },
      {
        id: 'fc60356a-de90-4398-8bf0-ea86d82c8706',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. details.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T14:26:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: 'fb7a4188-1c8c-4b10-9b5b-d0515a9ae6a3',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. delight.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T16:21:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5'
      },
      {
        id: 'cf227874-7ebf-4fe6-982d-8a9859c67607',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. a soothing symphony of sound.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T16:23:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: '3e1f49a3-be8b-4f69-a718-ce5241632787',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.through the garden, creating a fragrant paradise.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T16:25:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: 'dac065fe-1b34-47aa-aba9-95229f215a57',
        body: 'dotted the darkness.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-07-02T16:25:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b5'
      }
    ]
  },
  {
    id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b2gr',
    participants: [
      {
        status: 'online',
        id: 'DUMP_USER_ID',
        name: 'Jaydon Frankie',
        lastActivity: '2023-07-02T16:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-person-with-glasses-half-shaved-head_23-2149436187.jpg'
      },
      {
        status: 'offline',
        id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b7',
        name: 'Cristopher Cardenas',
        lastActivity: '2023-06-26T10:26:02.672Z',
        avatarUrl:
          'https://img.freepik.com/free-psd/3d-illustration-business-man-with-glasses_23-2149436194.jpg'
      },
      {
        status: 'online',
        id: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b8',
        name: 'Melanie Noble',
        lastActivity: '2023-06-25T09:26:02.672Z',
        avatarUrl:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmslNPLN14ZJrDjw2Zpg3m6JQXijkuC7DDKtMR0QWq5E9f_a9RrjJ87iQyhQL4PVS7k3M&usqp=CAU'
      }
    ],
    type: 'GROUP',
    unreadCount: 2,
    messages: [
      {
        id: '7fa3fdc8-12f9-4a87-8621-d7a327c72d85',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. delight.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-06-29T13:56:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: '52dca05d-3520-4c58-afbd-11867a51a514',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. symphony of sound.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-06-29T13:57:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b10'
      },
      {
        id: '21f4db64-eddb-47f2-9d25-69914ee09d43',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry blooming flowers wafted through the garden, creating a fragrant paradise.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-06-29T13:58:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b11'
      },
      {
        id: '4327b838-5cc7-449f-b2ff-5221184b03d0',
        body: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry darkness.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-06-29T13:59:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b9'
      },
      {
        id: 'f7997e40-73dd-43ed-90aa-5a40057cf3f4',
        attachments: [],
        body: 'Lorem Ipsum is simply dummy text of the students with thought-provoking ideas.',
        contentType: 'text',
        createdAt: '2023-06-29T14:00:02.673Z',
        senderId: 'DUMP_USER_ID'
      },
      {
        id: '431a005c-f93f-4da8-8923-ec6f052aa548',
        body: 'Lorem Ipsum is simply dummy text.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-06-29T16:26:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b7'
      },
      {
        id: '6833f4b8-3c46-42e5-a41e-ea0aa7c95255',
        body: 'Lorem Ipsum is simply dummy text slender proboscis.',
        contentType: 'text',
        attachments: [],
        createdAt: '2023-06-29T16:26:02.673Z',
        senderId: 'e99f09a7-dd88-49d5-b1c8-1daf80c2d7b8'
      }
    ]
  }
];
export default conversations;
