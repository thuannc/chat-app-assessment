import React from "react";
import { BrowserRouter } from 'react-router-dom';

import { AuthProvider } from './context/AuthProvider';
import { SocketProvider } from './context/SocketProvider';
import { UploadProvider } from './context/UploadManagerProvider';
import ThemeProvider from './theme';
import Router from './routes';

function App() {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <SocketProvider>
          <AuthProvider>
            <UploadProvider>
              <ThemeProvider>
                <Router />
              </ThemeProvider>
            </UploadProvider>
          </AuthProvider>
        </SocketProvider>
      </BrowserRouter>
    </React.StrictMode>
  );
}

export default App;
