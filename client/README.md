# README

# Install packages

```bash
npm i --force
```

## Run source với webpack

```jsx
npm run start:webpack
npm run build:webpack
```

# Cấu trúc thư mục

![Untitled](README/Untitled.png)

- components: chứa các component sử dụng chung trong dự án
- conststant: chứa các hằng số như chatEvents
- context: chứa các React Context global state cho dự án
  - AuthProvider: chứa thông tin đăng nhập của người dùng
  - ChatProvider: chứa state và method của luồng chat
  - SocketProvider: cung cấp khả năng truy cập nhanh đến socket instance
  - UploadManager: quản lí global upload
- mock: chứa data giả lập
- module: app được phân chia theo module giúp có thể tái sử dụng module ở nhiều nơi, trong mỗi module có components và services dùng riêng cho module đó
- pages: các màn hình chia theo route của app
- routes: cấu hình route của dự án, sử dụng react router v6
- theme: material theme override cho app, bao gồm: copmponens, màu sắc, font chữ. Em có một số config theme khá đẹp nên em đã dùng lại cho dự án này.
- types: app sử dụng typescript. Các interface và type được chứa trong thư mục này
- utils: chứa các hàm common về xử lí thời gian, generate uuid và socket instance

# Webpack config

```jsx
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: './src/index.tsx', // thiết lập file đầu vào
  devtool: 'inline-source-map', // giúp debug trong khi dev tránh bị minify
  output: {
    // cấu hình output sau khi build xong file được đặt trong folder dist
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js',
    publicPath: '/' // sử dụng để webpack cập nhập url bên trong CSS, HTML khi build trên môi trường
  },
  devServer: {
    static: './build',
    port: 3000,
    historyApiFallback: true
  },
  ignoreWarnings: [
    {
      module: /node_modules/
    }
  ],
  module: {
    // các module cần thiết cho các extension trong app
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'], // ensure compatibility with older browsers
            plugins: ['@babel/plugin-transform-object-assign'] // ensure compatibility with IE 11
          }
        },
        exclude: /node_modules/
      },
      {
        test: /\.html$/i,
        loader: 'html-loader'
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
        exclude: '/node_modules/'
      },
      {
        test: /\.svg$/i,
        type: 'asset'
      },
      {
        test: /\.(png|jpg|jpeg|svg)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'static/images/[hash][ext][query]'
        }
      }
    ]
  },
  optimization: {
    splitChunks: true // cấu hình Code splitting, em tat luon do chua biet config
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  plugins: [
    // HtmlWebpackPlugin: sắp xếp các file html theo một trật tự nhất định
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public', 'index.html')
    }),
    // process/browser để đọc được biến môi trường process.env
    new webpack.ProvidePlugin({
      process: 'process/browser'
    })
  ]
};
```

# package.json

```jsx
{
  "name": "chat-app",
  "version": "0.1.0",
  "description": "edvin assessment",
  "private": true,
  "dependencies": {
    "@emotion/react": "^11.11.1",
    "@emotion/styled": "^11.11.0",
    "@iconify/react": "^4.1.1",
    "@mui/material": "^5.13.6",
    "@types/node": "^16.18.38",
    "@types/react": "^18.2.14",
    "@types/react-dom": "^18.2.6",
    "date-fns": "^2.30.0",
    "lodash": "^4.17.21",
    "npm": "^9.7.2",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-lazy-load-image-component": "^1.6.0", // lazy load image
    "react-router-dom": "^6.14.1",
    "react-scripts": "5.0.1",
    "socket.io-client": "^4.7.1" // socket cho client
  },
  "scripts": {
    "start": "react-scripts start", // dev run react scripts
    "build": "react-scripts build",
    "start:webpack": "webpack serve --mode development --hot", // dev run với webpack
    "build:webpack": "webpack --mode production" // build với webpack
  },
  "eslintConfig": { // eslint config
    "extends": [
      "react-app"
    ]
  },
  "devDependencies": { // các dev devDependencies cho type script và webpack module
    "@babel/core": "^7.22.6",
    "@babel/plugin-transform-object-assign": "^7.22.5",
    "@babel/preset-env": "^7.22.6",
    "@types/autosuggest-highlight": "^3.2.0",
    "@types/lodash": "^4.14.195",
    "@types/react-lazy-load-image-component": "^1.5.3",
    "babel-loader": "^9.1.2",
    "html-loader": "^4.2.0",
    "html-webpack-plugin": "^5.5.3",
    "process": "^0.11.10",
    "ts-loader": "^9.4.4",
    "typescript": "^5.1.6",
    "webpack": "^5.88.1",
    "webpack-cli": "^5.1.4"
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}
```

# Dynamic import

![Untitled](README/Untitled%201.png)

# Hỗ trợ tìm contacts

![Untitled](README/Untitled%202.png)

# Quản lí upload theo kênh với UploadContext

![Untitled](README/Untitled%203.png)
