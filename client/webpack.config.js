const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: './src/index.tsx',
  devtool: 'inline-source-map',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.js',
    publicPath: '/' // https://stackoverflow.com/questions/50690137/cannot-go-directly-to-dynamic-route-with-react-router-and-webpack-dev-server
  },
  devServer: {
    static: './build',
    port: 3000,
    historyApiFallback: true // https://stackoverflow.com/questions/50690137/cannot-go-directly-to-dynamic-route-with-react-router-and-webpack-dev-server
  },
  ignoreWarnings: [
    {
      module: /node_modules/
    }
  ],
  module: {
    rules: [
      {
        test: /\.(ts|tsx)?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'], // ensure compatibility with older browsers
            plugins: ['@babel/plugin-transform-object-assign'] // ensure compatibility with IE 11
          }
        },
        exclude: /node_modules/
      },
      {
        test: /\.html$/i,
        loader: 'html-loader'
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
        exclude: '/node_modules/'
      },
      {
        test: /\.svg$/i,
        type: 'asset'
      },
      {
        test: /\.(png|jpg|jpeg|svg)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'static/images/[hash][ext][query]'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public', 'index.html')
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser'
    })
  ]
};
